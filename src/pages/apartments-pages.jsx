import {
  Center,
  Flex,
  Heading,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Spinner,
  useToast,
} from "@chakra-ui/react";
import ApartmentCard from "../components/apartment-card";
import { createContext, useEffect, useState } from "react";
import AccordionComponent from "../components/accordion";
import SortDropdownComponent from "../components/sort-dropdown";

export const ApartmentsSortingContext = createContext();
export const ApartmentsFilteringContext = createContext();
export const ApartmentsListContext = createContext();

export default function ApartmentsPage() {
  const toast = useToast();

  const [sorting, setSorting] = useState(null);
  const [filtering, setFiltering] = useState(0);
  const [isFetchning, setFetching] = useState(false);
  const [apartments, setApartments] = useState([]);

  async function getApartments(queryParamsStr) {
    setFetching(true);
    const allApartmentsGetResponse = await fetch(
      process.env.REACT_APP_API_HOST + "/apartments" + queryParamsStr,
      {
        method: "GET",
        headers: { "Content-Type": "application/json" },
      }
    );
    setFetching(false);
    if (allApartmentsGetResponse.status === 200) {
      const parsedApartmentsResponse = await allApartmentsGetResponse.json();
      setApartments(parsedApartmentsResponse);
      console.log("allApartmentsGetResponse", parsedApartmentsResponse);
    } else {
      toast({ description: "Sorry...Couldn't fetch apartments!" });
    }
  }

  useEffect(() => {
    let queryParamsStr = "";
    if (sorting || filtering) {
      queryParamsStr += "?";
      if (sorting) {
        queryParamsStr += "price=" + sorting.order + "&";
      }
      if (filtering) {
        queryParamsStr += "rooms=" + filtering;
      }
    }
    getApartments(queryParamsStr);
  }, [sorting, filtering]);

  return (
    <Flex as="section" direction="column" gap="2em">
      <Heading as="h1" size="lg">
        Apartments Rental
      </Heading>
      <ApartmentsListContext.Provider value={{ apartments, setApartments }}>
        <Flex as="section" direction="column">
          <Heading as="h2" size="md" marginBottom=".5em">
            New apartment creation form:
          </Heading>
          <AccordionComponent />
        </Flex>
        <Flex as="section" direction="column">
          <Heading as="h2" size="md" marginBottom=".5em">
            Total view of {apartments.length} apartments :
          </Heading>
          <ApartmentsSortingContext.Provider value={{ sorting, setSorting }}>
            <ApartmentsFilteringContext.Provider
              value={{ filtering, setFiltering }}
            >
              <Flex
                justifyContent="end"
                bgColor="ghostwhite"
                gap="1em"
                padding=".5em"
              >
                <SortDropdownComponent />
                <NumberInput
                  min={1}
                  pattern="[0-9]+"
                  onChange={(count) => setFiltering(count)}
                >
                  <NumberInputField bgColor="white" />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
              </Flex>
              <Flex direction="column" gap=".5em">
                {isFetchning ? (
                  <Center margin="2em">
                    <Spinner />
                  </Center>
                ) : (
                  apartments.map((apartment) => (
                    <ApartmentCard
                      apartmentDetails={apartment}
                      key={apartment.id}
                    />
                  ))
                )}
              </Flex>
            </ApartmentsFilteringContext.Provider>
          </ApartmentsSortingContext.Provider>
        </Flex>
      </ApartmentsListContext.Provider>
    </Flex>
  );
}

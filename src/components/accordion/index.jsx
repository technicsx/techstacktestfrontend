import { AddIcon, EditIcon } from "@chakra-ui/icons";
import {
  Accordion,
  AccordionButton,
  AccordionItem,
  AccordionPanel,
  Text,
} from "@chakra-ui/react";
import ApartmentForm from "../apartment-form";
import "./style.css";

export default function AccordionComponent({ apartment, setApartment }) {
  return (
    <Accordion backgroundColor="ghostwhite" allowToggle>
      <AccordionItem>
        <AccordionButton>
          <Text as="span" className="accordion-heading">
            {apartment ? "Edit apartment details" : "Add new apartment"}
          </Text>
          {apartment ? <EditIcon /> : <AddIcon />}
        </AccordionButton>
        <AccordionPanel pb={4}>
          <ApartmentForm
            apartmentDetails={apartment}
            setApartment={setApartment}
          />
        </AccordionPanel>
      </AccordionItem>
    </Accordion>
  );
}

import { Box, Button } from "@chakra-ui/react";
import { ApartmentsSortingContext } from "../../pages/apartments-pages";
import sortOrder from "./sort-order";
import { useContext } from "react";
import "./style.css";

export default function SortDropdownComponent() {
  const { setSorting } = useContext(ApartmentsSortingContext);
  async function setSort(order) {
      setSorting({ order });
  }
  return (
    <Box className="dropdown">
      <Button className="dropdown-btn">Sort by price</Button>
      <Box className="dropdown-content">
        <Button
          className="dropdown-content-btn"
          onClick={() => setSort(sortOrder.asc)}
        >
          Sort ASC
        </Button>
        <Button
          className="dropdown-content-btn"
          onClick={() => setSort(sortOrder.desc)}
        >
          Sort DESC
        </Button>
      </Box>
    </Box>
  );
}

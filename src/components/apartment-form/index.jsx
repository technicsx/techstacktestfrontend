import {
  Button,
  FormControl,
  FormLabel,
  Input,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Textarea,
  useToast,
} from "@chakra-ui/react";
import React, { useContext, useEffect, useState } from "react";
import { ApartmentsListContext } from "../../pages/apartments-pages";

export default function ApartmentForm({ apartmentDetails, setApartment }) {
  const toast = useToast();

  const [name, setName] = useState("");
  const [price, setPrice] = useState(0.0);
  const [rooms, setRooms] = useState(0);
  const [description, setDescription] = useState("");
  const [isSubmitting, setSubmitting] = useState(false);

  const { apartments, setApartments } = useContext(ApartmentsListContext);

  async function isValidApartmentDetails() {
    if (!name || name.length > 99) {
      toast({
        description:
          "Incorrect name format. Can’t be empty. Max length is 99 characters",
      });
      return false;
    }
    if (rooms <= 0) {
      toast({
        description: "Incorrect rooms format. Can’t be 0 or less than 0 ",
      });
      return false;
    }
    if (price <= 0) {
      toast({
        description: "Incorrect price format. Can’t be 0 or less than 0 ",
      });
      return false;
    }
    if (name.length > 999) {
      toast({
        description:
          "Incorrect description format. Can’t be empty. It is optional but max length is 999 characters",
      });
      return false;
    }
    return true;
  }
  async function submitApartment() {
    if (isValidApartmentDetails()) {
      setSubmitting(true);
      const apartment = { name, price, rooms, description };
      const apartmentSubmissionResponse = await fetch(
        process.env.REACT_APP_API_HOST +
          "/apartments" +
          (apartmentDetails ? "/" + apartmentDetails.id : ""),
        {
          method: apartmentDetails ? "PUT" : "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(apartment),
        }
      );
      setSubmitting(false);
      const status = apartmentSubmissionResponse.status;
      const data = await apartmentSubmissionResponse.json();
      if (status === 200 || status === 201) {
        if (apartmentDetails) {
          apartment.id = apartmentDetails.id;
          setApartment(apartment);
        } else {
          setName("");
          setDescription("");
          apartment.id = data.id;
          setApartments([...apartments, apartment]);
        }
        toast({ description: "Success!...Apartment was submitted" });
      } else {
        toast({
          description:
            "Couldn't submit the apartment!" +
            data.message,
        });
      }
    } else {
      toast({ description: "Try to follow format rules once more!" });
    }
  }

  useEffect(() => {
    setName(apartmentDetails?.name ?? "");
    setPrice(apartmentDetails?.price);
    setRooms(apartmentDetails?.rooms);
    setDescription(apartmentDetails?.description ?? "");
  }, []);

  return (
    <FormControl maxWidth="800px" marginLeft="auto" marginRight="auto">
      <FormLabel>Apartment name: </FormLabel>
      <Input
        bgColor="white"
        value={name}
        onChange={(e) => setName(e.target.value)}
        maxLength="99"
      />
      <FormLabel>Rooms count:</FormLabel>
      <NumberInput min={1} value={rooms} onChange={setRooms}>
        <NumberInputField bgColor="white" />
        <NumberInputStepper>
          <NumberIncrementStepper />
          <NumberDecrementStepper />
        </NumberInputStepper>
      </NumberInput>
      <FormLabel>Price:</FormLabel>
      <NumberInput
        bgColor="white"
        min={0.1}
        pattern="[0-9]+(\.[0-9]+)?"
        value={price}
        onChange={setPrice}
      >
        <NumberInputField />
      </NumberInput>
      <FormLabel>Description: {apartmentDetails ? "" : "(Optional)"}</FormLabel>
      <Textarea
        bgColor="white"
        placeholder="Write a little about the apartment..."
        value={description}
        onChange={(e) => setDescription(e.target.value)}
        maxLength="999"
      />
      <Button
        marginTop="1em"
        bgColor="blue.200"
        color="white"
        onClick={submitApartment}
        isLoading={isSubmitting}
        type="submit"
      >
        {apartmentDetails ? "Edit the apartment" : "Add new apartment"}
      </Button>
    </FormControl>
  );
}

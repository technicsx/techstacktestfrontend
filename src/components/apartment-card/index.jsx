import { Badge, Button, Flex, Text, useToast } from "@chakra-ui/react";
import { Heading } from "@chakra-ui/react";
import React, { createContext, useContext, useState } from "react";
import AccordionComponent from "../accordion";
import "./style.css";
import { ApartmentsListContext } from "../../pages/apartments-pages";
export const ApartmentDetailsContext = createContext();

export default function ApartmentCard({ apartmentDetails }) {
  const toast = useToast();

  const [apartment, setApartment] = useState(apartmentDetails);
  const [isDeleting, setDeleting] = useState(false);

  const { apartments, setApartments } = useContext(ApartmentsListContext);

  async function deleteApartment() {
    setDeleting(true);
    const apartmentDeleteResponse = await fetch(
      process.env.REACT_APP_API_HOST + "/apartments/" + apartmentDetails.id,
      {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
      }
    );
    setDeleting(false);
    if (apartmentDeleteResponse.status === 200) {
      setApartments(
        apartments.filter((apartment) => apartment.id !== apartmentDetails.id)
      );
      toast({ description: "Success!...Apartment was deleted" });
    } else {
      toast({ description: "Sorry...Couldn't delete the apartment!" });
    }
  }

  return (
    <Flex className="outer-apartment-flex-card" shadow="md">
      <Flex as="article" className="inner-apartment-flex-card">
        <Flex as="aside" className="inner-left-card aside-card" boxShadow="sm">
          <Heading size="sm" textAlign="center">
            {apartment.name}
          </Heading>
          <Badge bgColor="lavender" textAlign="center">
            rooms: {apartment.rooms}
          </Badge>
          <Badge bgColor="green.300" color="white" textAlign="center">
            price: {apartment.price}$
          </Badge>
        </Flex>
        <Text className="apartment-description-text">
          {apartment.description
            ? apartment.description
            : "No description was added..."}
        </Text>
        <Flex as="aside" className="inner-right-card aside-card">
          <Button
            bgColor="red.300"
            color="white"
            width="100%"
            isLoading={isDeleting}
            onClick={() => deleteApartment()}
          >
            Delete
          </Button>
        </Flex>
      </Flex>
      <AccordionComponent apartment={apartment} setApartment={setApartment} />
    </Flex>
  );
}

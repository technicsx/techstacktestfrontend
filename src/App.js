import "./App.css";
import {
  ChakraProvider,
  Container,
} from "@chakra-ui/react";
import ApartmentsPage from "./pages/apartments-pages";

function App() {
  return (
    <ChakraProvider>
      <Container id="app-container">
        <ApartmentsPage />
      </Container>
    </ChakraProvider>
  );
}

export default App;
